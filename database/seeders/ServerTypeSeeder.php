<?php

namespace Database\Seeders;

use App\Models\ServerManufacture;
use App\Models\ServerType;
use Illuminate\Database\Seeder;

class ServerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dell = ServerManufacture::whereName('Dell' )->first() ?? ServerManufacture::factory()->dell()->create();
        $hp = ServerManufacture::whereName('HP' )->first() ?? ServerManufacture::factory()->hp()->create();

        // create dell available server types
        ServerType::factory()->r210()->create(['server_manufacture_id' => $dell]);
        ServerType::factory()->r730()->create(['server_manufacture_id' => $dell]);

        // create hp available server types
        ServerType::factory()->ml110()->create(['server_manufacture_id' => $hp]);
        ServerType::factory()->ml350()->create(['server_manufacture_id' => $hp]);
    }
}
