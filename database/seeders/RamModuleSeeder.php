<?php

namespace Database\Seeders;

use App\Models\RamModule;
use Illuminate\Database\Seeder;

class RamModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create ddr3 available modules
        RamModule::factory()->ddr3_1gb()->create();
        RamModule::factory()->ddr3_2gb()->create();
        RamModule::factory()->ddr3_4gb()->create();
        RamModule::factory()->ddr3_8gb()->create();

        // create ddr4 available modules
        RamModule::factory()->ddr4_1gb()->create();
        RamModule::factory()->ddr4_2gb()->create();
        RamModule::factory()->ddr4_4gb()->create();
        RamModule::factory()->ddr4_8gb()->create();
    }
}
