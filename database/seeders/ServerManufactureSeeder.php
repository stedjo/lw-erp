<?php

namespace Database\Seeders;

use App\Models\ServerManufacture;
use Illuminate\Database\Seeder;

class ServerManufactureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServerManufacture::factory()->dell()->create();
        ServerManufacture::factory()->hp()->create();
    }
}
