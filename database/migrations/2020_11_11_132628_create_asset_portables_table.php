<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetPortablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_portables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_asset_id');
            $table->unsignedInteger('portable_id');
            $table->string('portable_type');
            $table->timestamps();


            $table->foreign('company_asset_id')
                ->references('id')
                ->on('company_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_portables');
    }
}
