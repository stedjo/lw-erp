<?php

namespace Database\Factories;

use App\Models\RamModule;
use Illuminate\Database\Eloquent\Factories\Factory;

class RamModuleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RamModule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'type' => $this->faker->randomElement(['DDR3', 'DDR4']),
            'unit' => $this->faker->randomElement(['GB']),
            'size' => $this->faker->randomDigitNotNull,
        ];
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr3_1gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR3',
                'unit' => 'GB',
                'size' => 1,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr3_2gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR3',
                'unit' => 'GB',
                'size' => 2,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr3_4gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR3',
                'unit' => 'GB',
                'size' => 4,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr3_8gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR3',
                'unit' => 'GB',
                'size' => 8,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr4_1gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR4',
                'unit' => 'GB',
                'size' => 1,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr4_2gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR4',
                'unit' => 'GB',
                'size' => 2,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr4_4gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR4',
                'unit' => 'GB',
                'size' => 4,
            ];
        });
    }

    /**
     * @return RamModuleFactory
     */
    public function ddr4_8gb(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => 'DDR4',
                'unit' => 'GB',
                'size' => 8,
            ];
        });
    }
}
