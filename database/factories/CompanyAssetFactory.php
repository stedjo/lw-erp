<?php

namespace Database\Factories;

use App\Models\CompanyAsset;
use App\Models\ServerType;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompanyAssetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyAsset::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'uid' => $this->faker->unique()->randomNumber(),
            'price' => $this->faker->randomNumber(),
        ];
    }
}
