<?php

namespace Database\Factories;

use App\Models\ServerManufacture;
use App\Models\ServerType;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServerTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServerType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'server_manufacture_id' => ServerManufacture::factory(),
        ];
    }

    /**
     * @return ServerTypeFactory
     */
    public function r210(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'R210',
                'server_manufacture_id' => ServerManufacture::factory()->dell(),
            ];
        });
    }

    /**
     * @return ServerTypeFactory
     */
    public function r730(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'R730',
                'server_manufacture_id' => ServerManufacture::factory()->dell(),
            ];
        });
    }

    /**
     * @return ServerTypeFactory
     */
    public function ml110(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'ML110',
                'server_manufacture_id' => ServerManufacture::factory()->hp(),
            ];
        });
    }

    /**
     * @return ServerTypeFactory
     */
    public function ml350(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'ML350',
                'server_manufacture_id' => ServerManufacture::factory()->hp(),
            ];
        });
    }
}
