<?php

namespace Database\Factories;

use App\Models\ServerManufacture;
use Illuminate\Database\Eloquent\Factories\Factory;

class ServerManufactureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ServerManufacture::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->company,
        ];
    }

    /**
     * @return ServerManufactureFactory
     */
    public function dell(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'Dell',
            ];
        });
    }

    /**
     * @return ServerManufactureFactory
     */
    public function hp(): self
    {
        return $this->state(function (array $attributes) {
            return [
                'name' => 'HP',
            ];
        });
    }
}
