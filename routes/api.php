<?php

use App\Http\Controllers\AssetsController;
use App\Http\Controllers\Documentation\SwaggerController;
use App\Http\Controllers\ModulesController;
use App\Http\Controllers\ServersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('assets')->group(function () {
    Route::get('/', [AssetsController::class, 'index']);
    Route::get('/{asset}', [AssetsController::class, 'view']);
    Route::post('/', [AssetsController::class, 'create']);
    Route::delete('/{asset}', [AssetsController::class, 'delete']);
});

Route::prefix('modules')->group(function () {
    Route::get('/', [ModulesController::class, 'index']);
});

Route::prefix('servers')->group(function () {
    Route::get('/', [ServersController::class, 'index']);
    Route::get('/{manufacture}', [ServersController::class, 'view']);
});

Route::get('/doc/swagger', SwaggerController::class);
