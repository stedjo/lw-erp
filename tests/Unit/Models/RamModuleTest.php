<?php

namespace Tests\Unit\Models;

use App\Models\CompanyAsset;
use App\Models\RamModule;
use Illuminate\Support\Collection;
use Tests\TestCase;

class RamModuleTest extends TestCase
{
    /**
     * @return void
     */
    public function testCanGetAssets()
    {
        $ram = RamModule::factory()->ddr3_1gb()->create();
        $asset = CompanyAsset::factory()->create();

        $asset->modules()->attach($ram);

        $this->assertInstanceOf(Collection::class, $ram->assets);
        $this->assertCount(1, $ram->assets);
        $this->assertContainsOnlyInstancesOf(CompanyAsset::class, $ram->assets);
        $this->assertContains($asset->id, $ram->assets->pluck('id'));
    }
}
