<?php

namespace Tests\Unit\Models;

use App\Models\CompanyAsset;
use App\Models\RamModule;
use App\Models\ServerType;
use Illuminate\Support\Collection;
use Tests\TestCase;

class CompanyAssetTest extends TestCase
{
    /**
     * @return void
     */
    public function testCanGetServers()
    {
        $asset = CompanyAsset::factory()->create();
        $server = ServerType::factory()->r210()->create();

        $asset->servers()->attach($server);

        $this->assertInstanceOf(Collection::class, $asset->servers);
        $this->assertCount(1, $asset->servers);
        $this->assertContainsOnlyInstancesOf(ServerType::class, $asset->servers);
        $this->assertContains($server->id, $asset->servers->pluck('id'));
    }

    /**
     * @return void
     */
    public function testCanGetModules()
    {
        $asset = CompanyAsset::factory()->create();
        $ram1 = RamModule::factory()->ddr3_1gb()->create();
        $ram2 = RamModule::factory()->ddr3_2gb()->create();

        $asset->modules()->attach([$ram1->id, $ram2->id]);

        $this->assertInstanceOf(Collection::class, $asset->modules);
        $this->assertCount(2, $asset->modules);
        $this->assertContainsOnlyInstancesOf(RamModule::class, $asset->modules);
        $this->assertContains($ram1->id, $asset->modules->pluck('id'));
        $this->assertContains($ram2->id, $asset->modules->pluck('id'));
    }
}
