<?php

namespace Tests\Unit\Models;

use App\Models\ServerManufacture;
use App\Models\ServerType;
use Tests\TestCase;

class ServerTypeTest extends TestCase
{
    /**
     * @return void
     */
    public function testCanGetServerManufacture()
    {
        $manufacture = ServerManufacture::factory()->dell()->create();
        $server = ServerType::factory()->r210()->create(['server_manufacture_id' => $manufacture]);

        $this->assertInstanceOf(ServerManufacture::class, $server->serverManufacture);
        $this->assertTrue($manufacture->is($server->serverManufacture));
    }
}
