<?php

namespace Tests\Unit\Models;

use App\Models\ServerManufacture;
use App\Models\ServerType;
use Illuminate\Support\Collection;
use Tests\TestCase;

class ServerManufactureTest extends TestCase
{
    /**
     * @return void
     */
    public function testCanGetServerTypes()
    {
        $manufacture = ServerManufacture::factory()->dell()->create();
        $server = ServerType::factory()->r210()->create(['server_manufacture_id' => $manufacture]);

        $this->assertInstanceOf(Collection::class, $manufacture->serverTypes);
        $this->assertCount(1, $manufacture->serverTypes);
        $this->assertContainsOnlyInstancesOf(ServerType::class, $manufacture->serverTypes);
        $this->assertContains($server->id, $manufacture->serverTypes->pluck('id'));
    }
}
