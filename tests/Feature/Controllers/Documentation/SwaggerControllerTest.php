<?php

namespace Tests\Feature\Controllers\Documentation;

use Tests\TestCase;

class SwaggerControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function testAnnotationsAreNotBroken()
    {
        $response = $this->getJson('/api/doc/swagger');
        $response->assertStatus(200);

        $response->assertJson([
            'info' => [
                'title' => 'ERP app',
            ],
        ]);
    }
}
