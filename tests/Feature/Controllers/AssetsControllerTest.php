<?php

namespace Tests\Feature\Controllers;

use App\Models\CompanyAsset;
use App\Models\RamModule;
use App\Models\ServerType;
use Tests\TestCase;

class AssetsControllerTest extends TestCase
{
    /**
     * @return void
     */
    public function testCanGetSingleAsset()
    {
        $asset = CompanyAsset::factory()->create();
        $server = ServerType::factory()->r210()->create();
        $ram1 = RamModule::factory()->ddr3_1gb()->create();
        $ram2 = RamModule::factory()->ddr3_2gb()->create();

        $asset->servers()->attach($server);
        $asset->modules()->attach([$ram1->id, $ram2->id]);

        $response = $this->getJson('api/assets/' . $asset->uid);

        $response->assertStatus(200);

        $response->assertExactJson([
            'data' => [
                'id' => $asset->id,
                'uid' => $asset->uid,
                'price' => $asset->price,
                'servers' => [
                    [
                        'id' => $server->id,
                        'name' => $server->name,
                        'server_manufacture' => [
                            'id' => $server->serverManufacture->id,
                            'name' => $server->serverManufacture->name,
                            'created_at' => $server->serverManufacture->created_at->toIso8601String(),
                            'updated_at' => $server->serverManufacture->updated_at->toIso8601String(),
                        ],
                        'created_at' => $server->created_at->toIso8601String(),
                        'updated_at' => $server->updated_at->toIso8601String(),
                    ],
                ],
                'modules' => [
                    [
                        'id' => $ram1->id,
                        'type' => $ram1->type,
                        'unit' => $ram1->unit,
                        'size' => $ram1->size,
                        'created_at' => $ram1->created_at->toIso8601String(),
                        'updated_at' => $ram1->updated_at->toIso8601String(),
                    ],
                    [
                        'id' => $ram2->id,
                        'type' => $ram2->type,
                        'unit' => $ram2->unit,
                        'size' => $ram2->size,
                        'created_at' => $ram2->created_at->toIso8601String(),
                        'updated_at' => $ram2->updated_at->toIso8601String(),
                    ],
                ],
                'created_at' => $asset->created_at->toIso8601String(),
                'updated_at' => $asset->updated_at->toIso8601String(),
            ],
        ]);
    }

    /**
     * @return void
     */
    public function testCanGetAllAssets()
    {
        $asset1 = CompanyAsset::factory()->create();
        $asset2 = CompanyAsset::factory()->create();
        $server = ServerType::factory()->r210()->create();
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $asset1->servers()->attach($server);
        $asset1->modules()->attach([$ram->id]);

        $asset2->servers()->attach($server);
        $asset2->modules()->attach([$ram->id]);

        $response = $this->getJson('api/assets');

        $response->assertStatus(200);

        $response->assertExactJson([
            'data' => [
                [
                    'id' => $asset1->id,
                    'uid' => $asset1->uid,
                    'price' => $asset1->price,
                    'servers' => [
                        [
                            'id' => $server->id,
                            'name' => $server->name,
                            'server_manufacture' => [
                                'id' => $server->serverManufacture->id,
                                'name' => $server->serverManufacture->name,
                                'created_at' => $server->serverManufacture->created_at->toIso8601String(),
                                'updated_at' => $server->serverManufacture->updated_at->toIso8601String(),
                            ],
                            'created_at' => $server->created_at->toIso8601String(),
                            'updated_at' => $server->updated_at->toIso8601String(),
                        ],
                    ],
                    'modules' => [
                        [
                            'id' => $ram->id,
                            'type' => $ram->type,
                            'unit' => $ram->unit,
                            'size' => $ram->size,
                            'created_at' => $ram->created_at->toIso8601String(),
                            'updated_at' => $ram->updated_at->toIso8601String(),
                        ],
                    ],
                    'created_at' => $asset1->created_at->toIso8601String(),
                    'updated_at' => $asset1->updated_at->toIso8601String(),
                ],
                [
                    'id' => $asset2->id,
                    'uid' => $asset2->uid,
                    'price' => $asset2->price,
                    'servers' => [
                        [
                            'id' => $server->id,
                            'name' => $server->name,
                            'server_manufacture' => [
                                'id' => $server->serverManufacture->id,
                                'name' => $server->serverManufacture->name,
                                'created_at' => $server->serverManufacture->created_at->toIso8601String(),
                                'updated_at' => $server->serverManufacture->updated_at->toIso8601String(),
                            ],
                            'created_at' => $server->created_at->toIso8601String(),
                            'updated_at' => $server->updated_at->toIso8601String(),
                        ],
                    ],
                    'modules' => [
                        [
                            'id' => $ram->id,
                            'type' => $ram->type,
                            'unit' => $ram->unit,
                            'size' => $ram->size,
                            'created_at' => $ram->created_at->toIso8601String(),
                            'updated_at' => $ram->updated_at->toIso8601String(),
                        ],
                    ],
                    'created_at' => $asset2->created_at->toIso8601String(),
                    'updated_at' => $asset2->updated_at->toIso8601String(),
                ],
            ],
        ]);
    }

    /**
     * @return void
     */
    public function testTryingToFetchNonExistingAssetIdWillReturn404()
    {
        $response = $this->getJson('api/assets/123');

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function testCanDeleteAnAsset()
    {
        $asset = CompanyAsset::factory()->create();
        $server = ServerType::factory()->r210()->create();
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $asset->servers()->attach($server);
        $asset->modules()->attach([$ram->id]);

        $response = $this->deleteJson('api/assets/' . $asset->uid);

        $response->assertStatus(204);

        $this->assertFalse(CompanyAsset::exists($asset->id));
        $this->assertEmpty($server->assets);
        $this->assertEmpty($ram->assets);
    }

    /**
     * @return void
     */
    public function testTryingToDeleteNonExistingAssetIdWillReturn404()
    {
        $response = $this->deleteJson('api/assets/123');

        $response->assertStatus(404);
    }

    /**
     * @return void
     */
    public function testCanCreateAnAsset()
    {
        $server = ServerType::factory()->r210()->create();
        $ram1 = RamModule::factory()->ddr3_1gb()->create();
        $ram2 = RamModule::factory()->ddr3_1gb()->create();

        $response = $this->postJson('api/assets', [
            'uid' => 123,
            'price' => 900,
            'server' => ['id' => $server->id],
            'modules' => [['id' => $ram1->id], ['id' => $ram2->id]],
        ]);

        $response->assertStatus(201);

        $this->assertTrue(CompanyAsset::where('uid', 123)->exists());
        $this->assertContains(123, $server->assets->pluck('uid'));
        $this->assertContains(123, $ram1->assets->pluck('uid'));
        $this->assertContains(123, $ram2->assets->pluck('uid'));
    }

    /**
     * @return void
     */
    public function testClientMustProvideUidWhenCreatingAsset()
    {
        $server = ServerType::factory()->r210()->create();
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $response = $this->postJson('api/assets', [
            'price' => 900,
            'server' => ['id' => $server->id],
            'modules' => [['id' => $ram->id]],
        ]);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testClientMustProvidePriceWhenCreatingAsset()
    {
        $server = ServerType::factory()->r210()->create();
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $response = $this->postJson('api/assets', [
            'uid' => 123,
            'server' => ['id' => $server->id],
            'modules' => [['id' => $ram->id]],
        ]);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testClientMustProvideServerIdWhenCreatingAsset()
    {
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $response = $this->postJson('api/assets', [
            'uid' => 123,
            'price' => 900,
            'modules' => [['id' => $ram->id]],
        ]);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testClientMustProvideValidServerIdWhenCreatingAsset()
    {
        $ram = RamModule::factory()->ddr3_1gb()->create();

        $response = $this->postJson('api/assets', [
            'uid' => 123,
            'price' => 900,
            'server' => ['id' => 456],
            'modules' => [['id' => $ram->id]],
        ]);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testClientMustProvideModuleIdsWhenCreatingAsset()
    {
        $server = ServerType::factory()->r210()->create();

        $response = $this->postJson('api/assets', [
            'price' => 900,
            'server' => ['id' => $server->id],
            'modules' => [],
        ]);

        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testClientMustProvideValidModuleIdsWhenCreatingAsset()
    {
        $server = ServerType::factory()->r210()->create();

        $response = $this->postJson('api/assets', [
            'uid' => 123,
            'price' => 900,
            'server' => ['id' => $server->id],
            'modules' => [['id' => 456]],
        ]);

        $response->assertStatus(422);
    }
}
