<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property int $server_manufacture_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property ServerManufacture $serverManufacture
 * @property Collection|CompanyAsset[] $assets
 *
 * Class ServerType
 * @package App\Models
 */
class ServerType extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo
     */
    public function serverManufacture(): BelongsTo
    {
        return $this->belongsTo(ServerManufacture::class);
    }

    /**
     * @return MorphToMany
     */
    public function assets(): MorphToMany
    {
        return $this->morphToMany(CompanyAsset::class, 'portable', 'asset_portables');
    }
}
