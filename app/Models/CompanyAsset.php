<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property int $uid
 * @property float $price
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|ServerType[] $servers
 * @property Collection|RamModule[] $modules
 *
 * Class CompanyAsset
 * @package App\Models
 */
class CompanyAsset extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['uid', 'price'];

    /**
     * @return MorphToMany
     */
    public function servers(): MorphToMany
    {
        return $this->morphedByMany(ServerType::class, 'portable', 'asset_portables');
    }

    /**
     * @return MorphToMany
     */
    public function modules(): MorphToMany
    {
        return $this->morphedByMany(RamModule::class, 'portable', 'asset_portables');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'uid';
    }
}
