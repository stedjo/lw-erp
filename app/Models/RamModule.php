<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $type
 * @property string $unit
 * @property int $size
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @method static Builder ofType(string $type)
 * @property Collection|CompanyAsset[] $assets
 *
 * Class RamModules
 * @package App\Models
 */
class RamModule extends Model
{
    use HasFactory;

    /**
     * @param Builder $query
     * @param string $type
     * @return Builder
     */
    public function scopeOfType(Builder $query, string $type): Builder
    {
        return $query->where('type', $type);
    }

    /**
     * @return MorphToMany
     */
    public function assets(): MorphToMany
    {
        return $this->morphToMany(CompanyAsset::class, 'portable', 'asset_portables');
    }
}
