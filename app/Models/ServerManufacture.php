<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|ServerType[] $serverTypes
 *
 * Class ServerManufacture
 * @package App\Models
 */
class ServerManufacture extends Model
{
    use HasFactory;

    /**
     * @return HasMany
     */
    public function serverTypes(): HasMany
    {
        return $this->hasMany(ServerType::class);
    }
}
