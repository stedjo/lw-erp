<?php

namespace App\Http\Serializers;

use League\Fractal\Serializer\DataArraySerializer as BaseDataArraySerializer;

class DataArraySerializer extends BaseDataArraySerializer
{
    /**
     * @param string $resourceKey
     * @param array $data
     * @return array
     */
    public function collection($resourceKey, array $data): array
    {
        return $this->addDataObject($resourceKey, $data);
    }

    /**
     * @param string $resourceKey
     * @param array $data
     * @return array
     */
    public function item($resourceKey, array $data): array
    {
        return $this->addDataObject($resourceKey, $data);
    }

    /**
     * This somewhat sloppy trick makes sure that when we explicitly give a false as resource key we don't add
     * it as a data section (thus preventing having nested 'data' sections when including child transformers
     *
     * @param mixed $resourceKey
     * @param array $data
     *
     * @return array
     */
    private function addDataObject($resourceKey, array $data): array
    {
        if ($resourceKey === false) {
            return $data;
        }

        return ['data' => $data];
    }
}
