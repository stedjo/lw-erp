<?php

namespace App\Http\Controllers\Documentation;

use Illuminate\Routing\Controller;

/**
 * Class SwaggerController.
 * @OA\OpenApi (
 *     @OA\Info(
 *         title="ERP app",
 *         version="1.0",
 *         @OA\Contact(
 *             email="hello@stefandjokic.com"
 *         )
 *     ),
 *     @OA\Server(
 *         url="http://localhost:8080",
 *         description="Local port server",
 *     ),
 * )
 *
 *
 * @codeCoverageIgnore
 */
class SwaggerController extends Controller
{
    public function __invoke()
    {
        return \OpenApi\scan([base_path('app')]);
    }
}
