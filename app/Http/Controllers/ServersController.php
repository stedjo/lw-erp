<?php

namespace App\Http\Controllers;

use App\Http\Transformers\ServerManufacture as ServerManufactureTransformer;
use App\Http\Transformers\ServerType;
use App\Models\ServerManufacture;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ServersController extends BaseController
{
    /**
     * @OA\Get(
     *     path="/api/servers",
     *     tags={"servers"},
     *     operationId="getAllServerManufactures",
     *     summary="Get all server manufactures",
     *     description="Retrieve all server manufactures",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ServersResponseData")
     *     ),
     * )
     *
     * @OA\Schema(
     *     schema="ServersResponseData",
     *     @OA\Property(
     *         property="data",
     *         type="array",
     *         description="The collection of server manufactures",
     *         @OA\Items(ref="#/components/schemas/ManufacturesResponse")
     *     ),
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return fractal(ServerManufacture::all(), app(ServerManufactureTransformer::class))->respond(JsonResponse::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/api/servers/{manufacture}",
     *     tags={"servers"},
     *     operationId="getServerTypes",
     *     summary="Get server types",
     *     description="Retrieve server types for given manufacture id",
     *     @OA\Parameter(
     *          in="path",
     *          name="manufacture",
     *          required=true,
     *          description="Id of the manufacture",
     *          @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ServerResponseData")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Invalid manufacture id",
     *     ),
     * )
     *
     * @OA\Schema(
     *     schema="ServerResponseData",
     *     @OA\Property(
     *         property="data",
     *         type="array",
     *         description="The collection of server types",
     *         @OA\Items(ref="#/components/schemas/ServersResponse")
     *     ),
     * )
     *
     * @param ServerManufacture $manufacture
     * @return JsonResponse
     */
    public function view(ServerManufacture $manufacture): JsonResponse
    {
        return fractal($manufacture->serverTypes, app(ServerType::class))->respond(JsonResponse::HTTP_OK);
    }
}
