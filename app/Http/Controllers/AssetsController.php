<?php

namespace App\Http\Controllers;

use App\Http\Transformers\CompanyAsset as CompanyAssetTransformer;
use App\Models\CompanyAsset;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class AssetsController extends BaseController
{
    /**
     * @OA\Get(
     *     path="/api/assets",
     *     tags={"assets"},
     *     operationId="getAllAssets",
     *     summary="Get all assets",
     *     description="Retrieve all assets and their relations",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/AssetsResponseData")
     *     ),
     * )
     *
     * @OA\Schema(
     *     schema="AssetsResponseData",
     *     @OA\Property(
     *         property="data",
     *         type="array",
     *         description="The collection of assets",
     *         @OA\Items(ref="#/components/schemas/AssetResponse")
     *     ),
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return fractal(CompanyAsset::all(), app(CompanyAssetTransformer::class))->respond(JsonResponse::HTTP_OK);
    }

    /**
     * @OA\Get(
     *     path="/api/assets/{asset}",
     *     tags={"assets"},
     *     operationId="getAsset",
     *     summary="Get asset",
     *     description="Retrieve asset and its relations for given asset id",
     *     @OA\Parameter(
     *          in="path",
     *          name="asset",
     *          required=true,
     *          description="Id of the asset",
     *          @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/AssetResponseData")
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Invalid asset id",
     *     ),
     * )
     *
     * @OA\Schema(
     *     schema="AssetResponseData",
     *     @OA\Property(
     *         property="data",
     *         description="Asset data",
     *         ref="#/components/schemas/AssetResponse"
     *     ),
     * )
     *
     * @param CompanyAsset $asset
     * @return JsonResponse
     */
    public function view(CompanyAsset $asset): JsonResponse
    {
        return fractal($asset, app(CompanyAssetTransformer::class))->respond(JsonResponse::HTTP_OK);
    }

    /**
     * @OA\Post(
     *     path="/api/assets",
     *     tags={"assets"},
     *     operationId="createNewAsset",
     *     summary="Create new asset",
     *     description="Will create new asset and portables relations",
     *     @OA\RequestBody(
     *         description="Asset data",
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/AssetRequest")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No content. Operation succesfully executed"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Invalid request",
     *     ),
     * )
     *
     * @OA\Schema(
     *   schema="AssetRequest",
     *   title="Asset request object",
     *   description="Asset request object",
     *   @OA\Property(
     *     property="uid",
     *     type="number"
     *   ),
     *   @OA\Property(
     *     property="price",
     *     type="number"
     *   ),
     *   @OA\Property(
     *     property="server",
     *     ref="#/components/schemas/AssetServerRequest"
     *   ),
     *   @OA\Property(
     *     property="modules",
     *     type="array",
     *     @OA\Items(ref="#/components/schemas/AssetModulesRequest")
     *   )
     * )
     *
     * @OA\Schema(
     *     schema="AssetServerRequest",
     *     required={"id"},
     *   @OA\Property(
     *     property="id",
     *     type="integer",
     *   ),
     * )
     *
     * @OA\Schema(
     *     schema="AssetModulesRequest",
     *     required={"id"},
     *   @OA\Property(
     *     property="id",
     *     type="integer",
     *   ),
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $request->validate([
            'uid' => ['required', 'unique:company_assets,uid', 'numeric'],
            'price' => ['required', 'numeric', 'not_in:0'],
            'server.id' => ['required', 'exists:server_types,id'],
            'modules' => ['required', 'array', 'min:1'],
            'modules.*.id' => ['required', 'exists:ram_modules'],
        ]);

        $asset = CompanyAsset::create(
            $request->only(['uid', 'price'])
        );

        $asset->servers()->attach($request->input('server.id'));
        $asset->modules()->attach($request->input('modules.*.id'));

        return response()->json([], JsonResponse::HTTP_CREATED);
    }

    /**
     * @OA\Delete(
     *     path="/api/assets/{asset}",
     *     tags={"assets"},
     *     operationId="deleteAsset",
     *     summary="Delete asset",
     *     description="Delete asset and its relations for given asset id",
     *     @OA\Parameter(
     *          in="path",
     *          name="asset",
     *          required=true,
     *          description="Id of the asset",
     *          @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="No content. Operation succesfully executed",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Invalid asset id",
     *     ),
     * )
     *
     * @param CompanyAsset $asset
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(CompanyAsset $asset): JsonResponse
    {
        $asset->servers()->detach();
        $asset->modules()->detach();
        $asset->delete();

        return response()->json([], JsonResponse::HTTP_NO_CONTENT);
    }
}
