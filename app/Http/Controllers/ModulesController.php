<?php

namespace App\Http\Controllers;

use App\Http\Transformers\RamModule as RamModuleTransformer;
use App\Models\RamModule;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class ModulesController extends BaseController
{
    /**
     * @OA\Get(
     *     path="/api/modules",
     *     tags={"modules"},
     *     operationId="getAllModules",
     *     summary="Get all modules",
     *     description="Retrieve all modules",
     *     @OA\Response(
     *         response=200,
     *         description="Success",
     *         @OA\JsonContent(ref="#/components/schemas/ModulesResponseData")
     *     ),
     * )
     *
     * @OA\Schema(
     *     schema="ModulesResponseData",
     *     @OA\Property(
     *         property="data",
     *         type="array",
     *         description="The collection of modules",
     *         @OA\Items(ref="#/components/schemas/ModulesResponse")
     *     ),
     * )
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return fractal(RamModule::all(), app(RamModuleTransformer::class))->respond(JsonResponse::HTTP_OK);
    }
}
