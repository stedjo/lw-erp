<?php

namespace App\Http\Transformers;

use App\Models\ServerType as ServerTypeModel;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *   schema="ServersResponse",
 *   title="Asset server response object",
 *   description="Asset server response object",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   ),
 *   @OA\Property(
 *     property="name",
 *     type="string",
 *     format="string"
 *   ),
 *   @OA\Property(
 *     property="server_manufacture",
 *     ref="#/components/schemas/ManufacturesResponse"
 *   ),
 *   @OA\Property(
 *     property="created_at",
 *     type="string",
 *     format="date-time"
 *   ),
 *   @OA\Property(
 *     property="updated_at",
 *     type="string",
 *     format="date-time"
 *   ),
 * )
 *
 * Class ServerType
 * @package App\Http\Transformers
 */
class ServerType extends TransformerAbstract
{
    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = ['server_manufacture'];

    /**
     * @param ServerTypeModel $server
     * @return array
     */
    public function transform(ServerTypeModel $server): array
    {
        return [
            'id' => $server->id,
            'name' => $server->name,
            'created_at' => $server->created_at->toIso8601String(),
            'updated_at' => $server->updated_at->toIso8601String(),
        ];
    }

    /**
     * @param ServerTypeModel $server
     * @return Item
     */
    public function includeServerManufacture(ServerTypeModel $server): Item
    {
        return $this->item($server->serverManufacture, app(ServerManufacture::class), false);
    }
}
