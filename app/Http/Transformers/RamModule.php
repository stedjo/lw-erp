<?php

namespace App\Http\Transformers;

use App\Models\RamModule as RamModuleModel;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *   schema="ModulesResponse",
 *   title="Asset modules response object",
 *   description="Asset modules response object",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   ),
 *   @OA\Property(
 *     property="type",
 *     type="string",
 *     format="string"
 *   ),
 *   @OA\Property(
 *     property="unit",
 *     type="string",
 *     format="string"
 *   ),
 *   @OA\Property(
 *     property="size",
 *     type="number",
 *   ),
 *   @OA\Property(
 *     property="created_at",
 *     type="string",
 *     format="date-time"
 *   ),
 *   @OA\Property(
 *     property="updated_at",
 *     type="string",
 *     format="date-time"
 *   ),
 * )
 *
 * Class RamModule
 * @package App\Http\Transformers
 */
class RamModule extends TransformerAbstract
{
    /**
     * @param RamModuleModel $module
     * @return array
     */
    public function transform(RamModuleModel $module): array
    {
        return [
            'id' => $module->id,
            'type' => $module->type,
            'unit' => $module->unit,
            'size' => $module->size,
            'created_at' => $module->created_at->toIso8601String(),
            'updated_at' => $module->updated_at->toIso8601String(),
        ];
    }
}
