<?php

namespace App\Http\Transformers;

use App\Models\CompanyAsset as CompanyAssetModel;
use League\Fractal\Resource\Collection;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *   schema="AssetResponse",
 *   title="Asset response object",
 *   description="Asset response object",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   ),
 *   @OA\Property(
 *     property="uid",
 *     type="number"
 *   ),
 *   @OA\Property(
 *     property="price",
 *     type="number"
 *   ),
 *   @OA\Property(
 *     property="servers",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/ServersResponse")
 *   ),
 *   @OA\Property(
 *     property="modules",
 *     type="array",
 *     @OA\Items(ref="#/components/schemas/ModulesResponse")
 *   ),
 *   @OA\Property(
 *     property="created_at",
 *     type="string",
 *     format="date-time"
 *   ),
 *   @OA\Property(
 *     property="updated_at",
 *     type="string",
 *     format="date-time"
 *   ),
 * )
 *
 * Class CompanyAsset
 * @package App\Http\Transformers
 */
class CompanyAsset extends TransformerAbstract
{
    /**
     * Include resources without needing it to be requested.
     *
     * @var array
     */
    protected $defaultIncludes = [
        'servers',
        'modules',
    ];

    /**
     * @param CompanyAssetModel $asset
     * @return array
     */
    public function transform(CompanyAssetModel $asset): array
    {
        return [
            'id' => $asset->id,
            'uid' => $asset->uid,
            'price' => $asset->price,
            'created_at' => $asset->created_at->toIso8601String(),
            'updated_at' => $asset->updated_at->toIso8601String(),
        ];
    }

    /**
     * @param CompanyAssetModel $asset
     * @return Collection
     */
    public function includeServers(CompanyAssetModel $asset): Collection
    {
        return $this->collection($asset->servers, app(ServerType::class), false);
    }

    /**
     * @param CompanyAssetModel $asset
     * @return Collection
     */
    public function includeModules(CompanyAssetModel $asset): Collection
    {
        return $this->collection($asset->modules, app(RamModule::class), false);
    }
}
