<?php

namespace App\Http\Transformers;

use App\Models\ServerManufacture as ServerManufactureModel;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Schema(
 *   schema="ManufacturesResponse",
 *   title="Server manufacture response object",
 *   description="Server manufacture response object",
 *   @OA\Property(
 *     property="id",
 *     type="integer"
 *   ),
 *   @OA\Property(
 *     property="name",
 *     type="string",
 *     format="string"
 *   ),
 *   @OA\Property(
 *     property="created_at",
 *     type="string",
 *     format="date-time"
 *   ),
 *   @OA\Property(
 *     property="updated_at",
 *     type="string",
 *     format="date-time"
 *   ),
 * )
 *
 * Class ServerManufacture
 * @package App\Http\Transformers
 */
class ServerManufacture extends TransformerAbstract
{
    /**
     * @param ServerManufactureModel $server
     * @return array
     */
    public function transform(ServerManufactureModel $server): array
    {
        return [
            'id' => $server->id,
            'name' => $server->name,
            'created_at' => $server->created_at->toIso8601String(),
            'updated_at' => $server->updated_at->toIso8601String(),
        ];
    }
}
