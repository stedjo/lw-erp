<?php

namespace App\Providers;

use App\Models\RamModule;
use App\Models\ServerType;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'server_type' => ServerType::class,
            'ram_module' => RamModule::class,
        ]);
    }
}
