import AllAssets from "./components/AllAssets";
import AddAsset from "./components/AddAsset";

export const routes = [
    {
        name: 'home',
        path: '/',
        component: AllAssets
    },
    {
        name: 'add',
        path: '/add',
        component: AddAsset
    }
];
