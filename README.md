## Assignment

As a developer, you are required to build the foundation of a new ERP application that will be used by employees to keep track of the servers as well as RAM modules.
Following are the business requirements.

1. The solution must allow users to enter following information about servers:
    * AssetId (number, which can be filled in by the user), e.g. 12345689
    * Brand (e.g. Dell, HP,..)
    * Name (R210, R730, ...)
    * Price (currency can be ignored)
2. The AssetId is unique
3. Each server has a list of RAM modules (e.g. 2x4GB + 1x8GB)
4. The solution must allow users to enter following information about RAM modules per server:
    * The price of a server is >0
    * Size (always in GigaByte, e.g. 1, 2, 4, 8,...)
5. Each server has at least 1 RAM module
6. The price of a server is >0

## Technical requirements

The solution must:

1. Be written in PHP using one of the following PHP frameworks: Symfony, Laravel, Zend (preferably Symfony)
2. Store its data in a MySQL database
3. Provide RESTful API to read/create/delete servers INCLUDING their RAM modules (Update operations are out of scope)
4. Be accessible through a simple, intuitive web interface (styling is not important, we are interested more in the functionality)

## Start application

* `docker-compose up -d`
* `docker-compose exec app composer install`
* `docker-compose exec app php artisan key:generate`
* `docker-compose exec app php artisan config:cache`
* `docker-compose exec app php artisan migrate:fresh --seed`
* `docker-compose exec app npm install`
* `docker-compose exec app npm run dev`
* Go to [http://localhost:8080](http://localhost:8080)

## Run test suite

* `docker-compose up -d`
* `docker-compose exec app composer install`
* `docker-compose exec app php artisan key:generate`
* `docker-compose exec app php artisan config:cache`
* `docker-compose exec app php artisan migrate:fresh --seed`
* `docker-compose exec app php artisan test` 

## Notes

* Test suite will truncate database, if you are using same build for tests and app make sure to run migration command in between  
* The api authentication/authorisation was not implemented since it is not in the scope of the assignment
* For open api specs of the endpoints go to [https://editor.swagger.io](https://editor.swagger.io) and use [http://localhost:8080/api/doc/swagger](http://localhost:8080/api/doc/swagger) as import url while running app containers
* You might find some code examples which come pre-installed with laravel framework
* Not all controllers are covered in test suite, the existing tests serve as PoC
* The code base was pushed in single commit so there is no git branching/message structure in place
* The frontend validation/error handling could be improved, the api is acting as a gatekeeper
* Had a lot of fun with this assignment, especially the frontend part  
